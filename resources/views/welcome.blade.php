<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="/">

    <title>Demo App</title>

    <link rel="icon" type="image/x-icon" href="favicon.ico">
</head>
<body>
<app-root></app-root>
<script type="text/javascript" src="/angular/dist/inline.bundle.js"></script>
<script type="text/javascript" src="/angular/dist/polyfills.bundle.js"></script>
<script type="text/javascript" src="/angular/dist/styles.bundle.js"></script>
<script type="text/javascript" src="/angular/dist/vendor.bundle.js"></script>
<script type="text/javascript" src="/angular/dist/main.bundle.js"></script>
</body>
</html>
