<?php

use Illuminate\Database\Seeder;
use App\Tomato;

class TomatoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tomatoes = array(
            ['name' => 'Red Tomato', 'color' => 'red'],
            ['name' => 'Cherry Tomato', 'color' => 'red'],
            ['name' => 'Green Zebra', 'color' => 'green'],
            ['name' => 'Valencia', 'color' => 'orange'],
            ['name' => 'Angular Tomato', 'color' => 'black'],
        );

        Tomato::insert($tomatoes);
    }
}
