<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            ['name' => 'John Doe', 'email' => 'user1@example.com', 'password' => Hash::make('secret')],
            ['name' => 'Jane Doe', 'email' => 'user2@example.com', 'password' => Hash::make('secret')],
            ['name' => 'Michael Scott', 'email' => 'user3@example.com', 'password' => Hash::make('secret')],
        );

        User::insert($users);
    }
}
