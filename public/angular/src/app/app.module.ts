import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MaterialModule} from './material.module';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {AuthGuard} from './guards/auth.guard';
import {AuthService} from './services/auth.service';
import {TomatoService} from './services/tomato.service';
import {TomatoComponent} from './tomato/tomato.component';
import { CreateComponent } from './tomato/create/create.component';

@NgModule({
    declarations: [
        AppComponent,
        LoginComponent,
        DashboardComponent,
        TomatoComponent,
        CreateComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        MaterialModule,
        AppRoutingModule,
        FormsModule,
    ],
    providers: [AuthGuard, AuthService, TomatoService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
