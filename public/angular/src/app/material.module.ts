/**
 * Material module imports and further exports material modules
 * that are later used in the app
 */

import {NgModule} from '@angular/core';
import {
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

const modules = [
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatListModule,
    MatMenuModule,
    MatInputModule,
    MatSelectModule,
    MatFormFieldModule,
    MatCardModule,
    MatIconModule,
];

@NgModule({
    imports: modules,
    exports: modules
})
export class MaterialModule {
}