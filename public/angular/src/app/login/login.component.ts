import {Component, OnInit} from '@angular/core';
import {AuthService} from '../services/auth.service';
import {Router} from '@angular/router';
import {User} from '../user';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

    model: User = new User();

    constructor(private authService: AuthService,
                private router: Router) {
    }

    ngOnInit() {
    }

    login() {
        this.authService.login(this.model)
            .subscribe(result => {
                console.log("result", result);
                if (result) {
                    console.log("Login successful");
                    this.router.navigate(['/dashboard']);
                } else {
                    console.log("Login failed");
                }
            });
    }

}
