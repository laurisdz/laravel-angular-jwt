import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TomatoComponent} from './tomato/tomato.component';
import {CreateComponent} from './tomato/create/create.component';
import {LoginComponent} from './login/login.component';
import {AuthGuard} from './guards/auth.guard';

const routes: Routes = [
    // landing page redirect
    {path: '', redirectTo: '/dashboard', pathMatch: 'full'},

    // login route, guest access
    {path: 'login', component: LoginComponent},

    // restricted routes
    {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
    {path: 'tomato/:id', component: TomatoComponent, canActivate: [AuthGuard]},
    {path: 'create', component: CreateComponent, canActivate: [AuthGuard]},
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
