export class Tomato {
    id: number;
    name: string;
    color: string;
}
