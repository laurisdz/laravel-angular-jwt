import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Tomato} from '../tomato';
import {Router} from '@angular/router';
import {TomatoService} from '../services/tomato.service';

@Component({
    selector: 'app-tomato',
    templateUrl: './tomato.component.html',
    styleUrls: ['./tomato.component.css']
})
export class TomatoComponent implements OnInit {

    @Input() tomato: Tomato;

    constructor(private route: ActivatedRoute,
                private tomatoService: TomatoService,
                private router: Router) {
    }

    ngOnInit(): void {
        // fetch tomato on component init
        this.getTomato();
    }

    getTomato(): void {
        const id = +this.route.snapshot.paramMap.get('id');
        this.tomatoService.find(id)
            .subscribe(tomato => this.tomato = tomato);
    }

    goBack(): void {
        this.router.navigate(['/tomato/' + this.tomato.id]);
    }

    save(): void {
        this.tomatoService.update(this.tomato)
            .subscribe(() => this.goBack());
    }

}
