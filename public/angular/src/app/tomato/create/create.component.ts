import {Component, OnInit, Input} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Tomato} from '../../tomato';
import {Router} from '@angular/router';
import {TomatoService} from '../../services/tomato.service';

@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent implements OnInit {

    @Input() tomato: Tomato = new Tomato();

    constructor(private route: ActivatedRoute,
                private tomatoService: TomatoService,
                private router: Router) {

    }

    ngOnInit() {
    }

    goBack(id: number): void {
        this.router.navigate(['/tomato/' + id]);
    }

    create(): void {
        this.tomatoService.create(this.tomato)
            .subscribe((tomato: Tomato) => this.goBack(tomato.id));
    }

}
