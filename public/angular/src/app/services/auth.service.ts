import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, map, tap} from 'rxjs/operators';
import {User} from '../user';

@Injectable()
export class AuthService {
    public token: string;
    private loginUrl = "/api/login";

    constructor(private http: HttpClient) {
        // set token if saved in local storage
        const currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.access_token;
    }

    login(user: User): Observable<User> {
        return this.http.post(this.loginUrl, user)
            .pipe(
                tap((response: User) => {
                    const token = response.access_token;
                    // if api returns token, log was successful
                    if (token) {
                        // store token in app
                        this.token = token;
                        localStorage.setItem(
                            'currentUser',
                            JSON.stringify(response)
                        );
                        return true;
                    }
                    // no token was returned, login failed
                    return false;
                }),
                catchError(this.handleError<any>('login'))
            )
    }

    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.log(`${operation} failed:`, error.statusText);

            // let the app keep running by returning an empty result.
            return of(result as T);
        };
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}