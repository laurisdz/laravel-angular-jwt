import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs/observable/of';
import {catchError, map, tap} from 'rxjs/operators';
import {Tomato} from '../tomato';
import {AuthService} from './auth.service';

@Injectable()
export class TomatoService {
    private listUrl = "/api/tomato";
    private tomatoUrl = "/api/tomato/";
    private createUrl = "/api/tomato";

    constructor(private http: HttpClient,
                private authService: AuthService) {
    }

    getOpts() {
        const header = new HttpHeaders().set('Authorization', 'Bearer' + this.authService.token);
        return {headers: header};
    }

    list(): Observable<Tomato[]> {
        return this.http.get(this.listUrl, this.getOpts())
            .pipe(
                tap(tomatoes => console.log('TomatoService.list', tomatoes)),
                catchError(this.handleError<any>('list'))
            )
    }

    find(id: number): Observable<Tomato> {
        return this.http.get(this.tomatoUrl + id, this.getOpts())
            .pipe(
                tap(_ => console.log(`TomatoService.find id=${id}`)),
                catchError(this.handleError<any>('find'))
            )
    }

    update(tomato: Tomato): Observable<any> {
        return this.http.put(this.tomatoUrl + tomato.id, tomato, this.getOpts()).pipe(
            tap(_ => console.log(`TomatoService.update id=${tomato.id}`)),
            catchError(this.handleError<any>('update'))
        );
    }

    create(tomato: Tomato): Observable<any> {
        return this.http.post(this.createUrl, tomato, this.getOpts()).pipe(
            tap(_ => console.log(`TomatoService.create id=${tomato.id}`)),
            catchError(this.handleError<any>('create'))
        );
    }

    /**
     * Handle failed requests
     * @param {string} operation
     * @param {T} result
     * @returns {(error: any) => Observable<T>}
     */
    private handleError<T>(operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
            console.log(`${operation} failed:`, error.statusText);

            // let the app keep running by returning an empty result.
            return of(result as T);
        };
    }
}