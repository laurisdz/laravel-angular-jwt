import {Component, OnInit} from '@angular/core';
import {TomatoService} from '../services/tomato.service';
import {Tomato} from '../tomato';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    public tomatoes: Tomato[];

    constructor(private tomatoService: TomatoService) {
    }

    ngOnInit() {
        // fetch all tomatos on component init
        this.tomatoService.list()
            .subscribe(tomatoes => this.tomatoes = tomatoes);
    }

}
