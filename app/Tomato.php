<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tomato extends Model
{
    // allow writing/updating only name and color
    protected $fillable = ['name', 'color'];

    // only show necessary columns
    protected $visible = ['id', 'name', 'color'];
}
