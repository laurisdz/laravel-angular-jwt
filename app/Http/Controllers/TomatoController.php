<?php

namespace App\Http\Controllers;

use App\Tomato;
use Illuminate\Http\Request;

class TomatoController extends Controller
{
    /**
     * TomatoController constructor
     */
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Tomato::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tomato = new Tomato($request->all());
        $tomato->save();
        return $tomato;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Tomato $tomato
     * @return \Illuminate\Http\Response
     */
    public function show(Tomato $tomato)
    {
        return $tomato;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Tomato $tomato
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tomato $tomato)
    {
        //
        $tomato->update($request->all());
        return $tomato;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Tomato $tomato
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tomato $tomato)
    {
        //
    }
}
